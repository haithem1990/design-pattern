package com.design.pattern.singleton;

public class Singleton {

    private static Singleton singleton;

    private int counter;

    private Singleton() {
        System.out.println("----- Initialize ----");
    }

    public static Singleton getInstance() {
        if (singleton == null)
            singleton = new Singleton();
        return singleton;
    }

    public void describe() {
        System.out.println("reference -----> " + singleton);
        System.out.println("counter -----> " + counter);
    }


    public synchronized void increment() {
        ++counter;
    }
}
