package com.design.pattern.uofw.person;

import java.util.List;

public interface PersonRepository {

    void add(Person person);

    void update(Person person);

    void deleteByFirstNameAndLastName(Person person);

    void addAll(List<Person> persons);

    void updateAll(List<Person> persons);

    void deleteAllByFirstNameAndLastName(List<Person> persons);
}
