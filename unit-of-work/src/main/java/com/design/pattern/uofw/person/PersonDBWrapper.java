package com.design.pattern.uofw.person;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class PersonDBWrapper {
    private static List<Person> persons = new LinkedList<>();

    public static void initialize() {
        System.out.println("######################## START INITIALIZATION ########################");
        Person person01 = new Person(10L, "Ali", "BenMohamed", 60);
        Person person02 = new Person(25L, "Mohamed", "Abdallah", 20);
        Person person03 = new Person(35L, "Ahmed", "BenAhmed", 45);
        persons.addAll(Arrays.asList(person01, person02, person03));
        describe();
        System.out.println("######################## END INITIALIZATION ########################");
    }

    public static void describe() {
        persons.forEach(person -> person.describe());
    }

    public static List<Person> getPersons() {
        return persons;
    }

    public static void setPersons(List<Person> persons) {
        PersonDBWrapper.persons = persons;
    }
}
