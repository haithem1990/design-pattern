package com.design.pattern.adapter.standard;

public interface Standard {

    void doStandardBusiness();
}
