package com.design.pattern.proxy.business;

public class BusinessProxy implements IBusiness {

    private IBusiness iBusiness;

    public Boolean doBusiness(String reference) {
        // initialize impl in the first call
        initializeImpl();
        System.out.println("BusinessProxy --> execute code before call ");
        Boolean result;
        try {
        result = this.iBusiness.doBusiness(reference);
        System.out.println("BusinessProxy --> execute code after returning ");
        System.out.println("BusinessProxy --> do validation of returned value ");
        validate(result);
        }catch (Exception e){
            System.out.println("BusinessProxy --> exception handling ");
            result = Boolean.FALSE;
        }
        System.out.println("BusinessProxy --> execute code after call");
        return result;
    }

    /**
     * for memory reason it is better to instantiate the impl in the first call
     */
    private void initializeImpl(){
        if (this.iBusiness==null){
        System.out.println(" !!!!!!!!!!!!!!!!!!!! BusinessProxy --> first call so instantiate the implementation !!!!!!!!!!!!!!!!");
        this.iBusiness = new BusinessImpl();
    }
    }

    private void validate(Boolean result){
         if(result){
        System.out.println("BusinessProxy --> congratulation good choice");
    }
        else {
        System.out.println("BusinessProxy --> try again with other reference");
        throw new IllegalArgumentException();
    }
    }
}
