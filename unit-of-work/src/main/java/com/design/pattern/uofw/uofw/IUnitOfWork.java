package com.design.pattern.uofw.uofw;

public interface IUnitOfWork<T> {

    void add(T t);

    void update(T t);

    void deleteByFirstNameAndLastName(T t);

    void work();
}
