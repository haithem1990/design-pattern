package com.design.pattern.template;

public abstract class Template {

    public void execute() {

        step1();
        step2();
    }

    public abstract void step1();

    public abstract void step2();
}
