package com.design.pattern.decorator.with;

import com.design.pattern.decorator.decorated.IDrink;

public class WithSugar extends Decorator {

    public WithSugar(IDrink iDrink) {
        super(iDrink);
    }

    public int getPrice() {
        return iDrink.getPrice() + 0;
    }

    public String getDescription() {
        return iDrink.getDescription() + " with sugar";
    }
}
