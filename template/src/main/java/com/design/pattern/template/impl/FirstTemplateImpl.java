package com.design.pattern.template.impl;

import com.design.pattern.template.Template;

public class FirstTemplateImpl extends Template {

    public void step1() {
        System.out.println("FirstTemplateImpl ---> step 1 ");
    }

    public void step2() {
        System.out.println("FirstTemplateImpl ---> step 2 ");
    }
}
