package com.design.pattern.decorator.with;

import com.design.pattern.decorator.decorated.IDrink;

public class WithCaramel extends Decorator {

    public WithCaramel(IDrink iDrink) {
        super(iDrink);
    }

    public int getPrice() {
        return iDrink.getPrice() + 10;
    }

    public String getDescription() {
        return iDrink.getDescription() + " with caramel";
    }
}
