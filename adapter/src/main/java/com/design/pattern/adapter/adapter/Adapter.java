package com.design.pattern.adapter.adapter;

import com.design.pattern.adapter.standard.Standard;
import com.design.pattern.adapter.target.TargetImpl;

public class Adapter implements Standard {

    private TargetImpl targetImpl;

    public Adapter(TargetImpl targetImpl) {
        this.targetImpl = targetImpl;
    }

    public void doStandardBusiness() {
        targetImpl.doCustomBusiness();
    }
}
