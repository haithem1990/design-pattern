package com.design.pattern.state.transition.impl;

import com.design.pattern.state.Plane;
import com.design.pattern.state.transition.StateTransition;

public class InGarage implements StateTransition {

    private final Plane plane;

    public InGarage(Plane plane) {
        this.plane = plane;
    }

    public void goOutFromGarage() {
        System.out.println("In Garage ----- > In Street ");
        plane.setStateTransition(new InStreet(plane));
    }

    public void fly() {
        System.out.println("It's impossible");
    }

    public void descent() {
        System.out.println("It's impossible");
    }

    public void enterToGarage() {
        System.out.println("Already in the garage");
    }

    public void describeState() {
        System.out.println("############# In Garage ####################");
    }
}
