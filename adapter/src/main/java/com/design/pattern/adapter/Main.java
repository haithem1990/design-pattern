package com.design.pattern.adapter;

import com.design.pattern.adapter.adapter.Adapter;
import com.design.pattern.adapter.target.TargetImpl;
import com.design.pattern.adapter.client.Client;
import com.design.pattern.adapter.standard.StandardImpl;

public class Main {

    public static void main(String[] args) {

        Client client;

        System.out.println("----------------------------------------");
        client = new Client(new StandardImpl());
        client.doClientBusiness();
        System.out.println("----------------------------------------");
        client = new Client(new Adapter(new TargetImpl()));
        client.doClientBusiness();
        System.out.println("----------------------------------------");

    }
}
