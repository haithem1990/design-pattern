package com.design.pattern.state.transition;

public interface StateTransition {
    // go out from the garage
    void goOutFromGarage();
    // fly
    void fly();
    // descent
    void descent();
    // enter to the garage
    void enterToGarage();
    // this method is responsible to describe the actual state
    void describeState();
}
