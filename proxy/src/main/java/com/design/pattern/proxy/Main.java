package com.design.pattern.proxy;

import com.design.pattern.proxy.business.BusinessImpl;
import com.design.pattern.proxy.business.BusinessProxy;
import com.design.pattern.proxy.client.Client;

public class Main {

    public static void main(String[] args) {

        Client client = new Client(new BusinessProxy());

        client.call("1");
        System.out.println("--------------------------------------------");
        client.call("2");
        System.out.println("--------------------------------------------");
        client.call("0");
    }
}
