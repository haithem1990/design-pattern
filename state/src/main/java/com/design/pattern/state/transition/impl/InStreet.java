package com.design.pattern.state.transition.impl;

import com.design.pattern.state.Plane;
import com.design.pattern.state.transition.StateTransition;

public class InStreet implements StateTransition {

    private final Plane plane;

    public InStreet(Plane plane) {
        this.plane = plane;
    }

    public void goOutFromGarage() {
        System.out.println("Already Out ");
    }

    public void fly() {
        System.out.println("In Street ----- > In Atmosphere ");
        plane.setStateTransition(new InAtmosphere(plane));
    }

    public void descent() {
        System.out.println("It's impossible");
    }

    public void enterToGarage() {
        System.out.println("In Street ----- > In Garage ");
        plane.setStateTransition(new InGarage(plane));
    }

    public void describeState() {
        System.out.println("############# In Street ####################");
    }
}
