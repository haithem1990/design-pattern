package com.design.pattern.proxy.business;

public interface IBusiness {

    Boolean doBusiness(String reference);
}
