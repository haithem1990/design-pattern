package com.design.pattern.decorator.decorated;

public interface IDrink {

    int getPrice();

    String getDescription();
}
