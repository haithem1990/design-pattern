package com.design.pattern.state;

public class Main {
    public static void main(String[] args) {
        Plane plane = new Plane();
        plane.describeState();
        plane.goOutFromGarage();
        plane.describeState();
        // #### start improve ### the same call of goOutFromGarage but the behaviour is changed because the state is also changed  #######
        plane.goOutFromGarage();
        plane.describeState();
        // ######### end improve the behaviour change
        plane.fly();
        plane.describeState();
        plane.descent();
        plane.describeState();
        plane.enterToGarage();
        plane.describeState();
    }
}
