package com.design.pattern.uofw;

import com.design.pattern.uofw.person.Person;
import com.design.pattern.uofw.person.PersonDBWrapper;
import com.design.pattern.uofw.person.PersonRepositoryImpl;
import com.design.pattern.uofw.uofw.IUnitOfWork;
import com.design.pattern.uofw.uofw.PersonUnitOfWork;

public class Main {

    public static void main(String[] args) {

        PersonDBWrapper.initialize();
        doFirstUnitWork();
        doSecondUnitWork();
    }

    private static void doFirstUnitWork() {
        IUnitOfWork iUnitOfWork = new PersonUnitOfWork(new PersonRepositoryImpl());
        try {
            iUnitOfWork.add(new Person(4L, "Steve", "Jonson", 19));
            iUnitOfWork.deleteByFirstNameAndLastName(new Person("Mohamed", "Abdallah"));
            iUnitOfWork.update(new Person(10L, "Salem", "Ben Ibrahim", 50));
            iUnitOfWork.work();
        } catch (Exception e) {
            System.out.printf("Error during unit of work ----> %s\n", e.getMessage());
        } finally {
            System.out.println("######################## PERSON DB DESCRIPTION AFTER UNIT OF WORK EXECUTION ########################");
            PersonDBWrapper.describe();
            System.out.println("######################## END ########################");
        }
    }

    private static void doSecondUnitWork() {
        IUnitOfWork iUnitOfWork = new PersonUnitOfWork(new PersonRepositoryImpl());
        try {
            iUnitOfWork.add(new Person(17L, "Rud", "Jonson", 55));
            iUnitOfWork.update(new Person(35L, "Josh", "Long", 35));
            // if we comment the call of deleteByFirstNameAndLastName this unit of work will be executed successfully
            iUnitOfWork.deleteByFirstNameAndLastName(new Person("Salem", "Ben Ibrahim"));
            iUnitOfWork.work();
        } catch (Exception e) {
            System.out.printf("Error during unit of work ----> %s\n", e.getMessage());
        } finally {
            System.out.println("######################## PERSON DB DESCRIPTION AFTER UNIT OF WORK EXECUTION ########################");
            PersonDBWrapper.describe();
            System.out.println("######################## END ########################");
        }
    }
}
