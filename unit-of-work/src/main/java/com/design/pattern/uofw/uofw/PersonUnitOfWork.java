package com.design.pattern.uofw.uofw;

import com.design.pattern.uofw.exception.ValidationOperationException;
import com.design.pattern.uofw.person.Person;
import com.design.pattern.uofw.person.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PersonUnitOfWork implements IUnitOfWork<Person> {

    private final PersonRepository personRepository;

    private final Map<Operation, List<Person>> personUnitOfWorkContext;

    public PersonUnitOfWork(PersonRepository personRepository) {
        this.personRepository = personRepository;
        this.personUnitOfWorkContext = new ConcurrentHashMap<>();
    }


    private void register(Person person, Operation operation) {
        List<Person> persons = (personUnitOfWorkContext.get(operation) == null ? new ArrayList<>() : personUnitOfWorkContext.get(operation));
        persons.add(person);
        personUnitOfWorkContext.put(operation, persons);
    }

    @Override
    public void add(Person person) {
        validateAddOperation(person);
        register(person, Operation.ADD);
    }

    @Override
    public void update(Person person) {
        validateUpdateOperation(person);
        register(person, Operation.UPDATE);
    }

    @Override
    public void deleteByFirstNameAndLastName(Person person) {
        validateDeleteOperation(person);
        register(person, Operation.DELETE_BY_FIRST_NAME_AND_LAST_NAME);
    }

    public synchronized void work() {
        System.out.println("######################## OK FOR ALL VALIDATION ########################");
        System.out.println("######################## WORK ########################");
        personRepository.addAll(personUnitOfWorkContext.get(Operation.ADD));
        personRepository.updateAll(personUnitOfWorkContext.get(Operation.UPDATE));
        personRepository.deleteAllByFirstNameAndLastName(personUnitOfWorkContext.get(Operation.DELETE_BY_FIRST_NAME_AND_LAST_NAME));
    }

    private void validateDeleteOperation(Person person) {
        if (person.getFirstName().startsWith("S")) {
            throw new ValidationOperationException("Delete is not supported for person's first name start with S");
        }
        if (person.getLastName().startsWith("Ben")) {
            throw new ValidationOperationException("Delete is not supported for person's last name start with Ben");
        }
    }

    private void validateAddOperation(Person person) {
        if (person.getAge() < 18) {
            throw new ValidationOperationException("Add person with age under 18 is not supported");
        }
        if (person.getId() < 1) {
            throw new ValidationOperationException("Add person with id under 1 is not supported ");
        }
    }

    private void validateUpdateOperation(Person person) {
        if (person.getAge() > 60) {
            throw new ValidationOperationException("Update person with age greater than 60 is not supported");
        }
    }
}
