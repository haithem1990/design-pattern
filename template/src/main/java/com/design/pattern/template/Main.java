package com.design.pattern.template;

import com.design.pattern.template.impl.FirstTemplateImpl;
import com.design.pattern.template.impl.SecondTemplateImpl;

public class Main {

    public static void main(String[] args) {
        System.out.println("-----------------------------------------");
        Template firstTemplate = new FirstTemplateImpl();
        firstTemplate.execute();
        System.out.println("-----------------------------------------");
        Template secondTemplate = new SecondTemplateImpl();
        secondTemplate.execute();
        System.out.println("-----------------------------------------");

    }
}
