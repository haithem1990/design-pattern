package com.design.pattern.proxy.client;

import com.design.pattern.proxy.business.IBusiness;

public class Client {

    private IBusiness iBusiness;

    public Client(IBusiness iBusiness) {
        this.iBusiness = iBusiness;
    }

    public void call(String reference){
        System.out.println("Client --> Client need to call businessImpl ");
        iBusiness.doBusiness(reference);
        System.out.println("Client -->  Good Job PROXY");
    }
}
