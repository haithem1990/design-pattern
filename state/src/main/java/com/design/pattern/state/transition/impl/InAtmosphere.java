package com.design.pattern.state.transition.impl;

import com.design.pattern.state.Plane;
import com.design.pattern.state.transition.StateTransition;

public class InAtmosphere implements StateTransition {

    private final Plane plane;

    public InAtmosphere(Plane plane) {
        this.plane = plane;
    }

    public void goOutFromGarage() {
        System.out.println("Already Out ");
    }

    public void fly() {
        System.out.println("Already flying ");
    }

    public void descent() {
        System.out.println("In Atmosphere ----- >  In Street ");
        plane.setStateTransition(new InStreet(plane));
    }

    public void enterToGarage() {
        System.out.println("Impossible");
    }

    public void describeState() {
        System.out.println("############# In Atmosphere ####################");
    }
}
