package com.design.pattern.uofw.person;

import java.util.List;
import java.util.stream.Collectors;

public class PersonRepositoryImpl implements PersonRepository {

    public void add(Person person) {
        PersonDBWrapper.getPersons().add(person);
    }

    public void update(Person person) {
        Person personTarget = PersonDBWrapper.getPersons().stream().filter(personItem -> personItem.getId().longValue() == person.getId().longValue()).findFirst().get();
        personTarget.setFirstName(person.getFirstName());
        personTarget.setLastName(person.getLastName());
        personTarget.setAge(person.getAge());
    }

    public void deleteByFirstNameAndLastName(Person person) {
        List<Person> persons = PersonDBWrapper.getPersons()
                .stream()
                .filter(
                        personItem -> !personItem.getFirstName().equals(person.getFirstName()) && !personItem.getLastName().equals(person.getLastName())
                )
                .collect(
                        Collectors.toList()
                );
        PersonDBWrapper.setPersons(persons);
    }

    @Override
    public void addAll(List<Person> persons) {
        if (persons != null && !persons.isEmpty()) {
            persons.forEach(person -> add(person));
        }
    }

    @Override
    public void updateAll(List<Person> persons) {
        if (persons != null && !persons.isEmpty()) {
            persons.forEach(person -> update(person));
        }
    }

    @Override
    public void deleteAllByFirstNameAndLastName(List<Person> persons) {
        if (persons != null && !persons.isEmpty()) {
            persons.forEach(person -> deleteByFirstNameAndLastName(person));
        }
    }
}
