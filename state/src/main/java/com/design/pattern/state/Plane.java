package com.design.pattern.state;

import com.design.pattern.state.transition.impl.InGarage;
import com.design.pattern.state.transition.StateTransition;

public class Plane {


    private StateTransition stateTransition;

    public Plane() {
        // the initial state is defined in the creation time
        this.stateTransition=new InGarage(this);
    }

    public void goOutFromGarage() {
        stateTransition.goOutFromGarage();
    }

    public void fly() {
        stateTransition.fly();
    }

    public void descent() {
        stateTransition.descent();
    }

    public void enterToGarage() {
        stateTransition.enterToGarage();
    }

    public void describeState() {
        stateTransition.describeState();
    }

    // getters and setters
    public StateTransition getStateTransition() {
        return stateTransition;
    }

    public void setStateTransition(StateTransition stateTransition) {
        this.stateTransition = stateTransition;
    }
}
