package com.design.pattern.uofw.exception;

public class ValidationOperationException extends RuntimeException {

    public ValidationOperationException(String message) {
        super(message);
    }
}
