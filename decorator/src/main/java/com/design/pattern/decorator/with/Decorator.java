package com.design.pattern.decorator.with;

import com.design.pattern.decorator.decorated.IDrink;

public abstract class Decorator implements IDrink {
    protected IDrink iDrink;

    public Decorator(IDrink iDrink) {
        this.iDrink = iDrink;
    }
}
