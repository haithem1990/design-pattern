package com.design.pattern.adapter.standard;

public class StandardImpl implements Standard {
    public void doStandardBusiness() {
        System.out.println("StandardImpl ----> doStandardBusiness");
    }
}
