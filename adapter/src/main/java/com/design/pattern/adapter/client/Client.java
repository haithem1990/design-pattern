package com.design.pattern.adapter.client;

import com.design.pattern.adapter.standard.Standard;

public class Client {

    private Standard standard;

    public Client(Standard standard) {
        this.standard = standard;
    }

    public void doClientBusiness(){
        System.out.println("Client ---> start doClientBusiness");
        standard.doStandardBusiness();
        System.out.println("Client ---> end doClientBusiness");
    }
}
