package com.design.pattern.uofw.uofw;

public enum Operation {

    ADD,
    UPDATE,
    DELETE_BY_FIRST_NAME_AND_LAST_NAME
}
