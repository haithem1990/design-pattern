package com.design.pattern.decorator.drink;

import com.design.pattern.decorator.decorated.IDrink;

public class Espresso implements IDrink {
    public int getPrice() {
        return 10;
    }

    public String getDescription() {
        return "Espresso";
    }
}
