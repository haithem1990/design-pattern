package com.design.pattern.decorator;

import com.design.pattern.decorator.decorated.IDrink;
import com.design.pattern.decorator.drink.Espresso;
import com.design.pattern.decorator.with.WithCaramel;
import com.design.pattern.decorator.with.WithMilk;
import com.design.pattern.decorator.with.WithSugar;

public class Main {

    public static void main(String[] args) {

        System.out.println("################  NOTE  ################");
        System.out.println("in decorator design pattern object can add behaviour to called methods");
        System.out.println("------- > object tell to the caller that if you call your object from my site i will add my own behaviour to your call");
        System.out.println("############################## ################");
        IDrink drink01 = new WithMilk(new WithSugar(new WithCaramel(new Espresso())));
        System.out.println(drink01.getDescription());
        System.out.println(drink01.getPrice());
        System.out.println("----------------------------------------");
        IDrink drink02 = new WithMilk(new WithSugar(new Espresso()));
        System.out.println(drink02.getDescription());
        System.out.println(drink02.getPrice());

        System.out.println("----------------------------------------");
        IDrink drink03 = new WithMilk(new Espresso());
        System.out.println(drink03.getDescription());
        System.out.println(drink03.getPrice());
    }

}
