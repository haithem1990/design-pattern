package com.design.pattern.singleton;

public class Main {

    public static void main(String[] args) {
        System.out.println("######## start Note ########");
        System.out.println("Singleton design pattern provide only one instance per Application");
        System.out.println("######## end Note ########");

        Singleton singleton01 = Singleton.getInstance();
        singleton01.describe();

        Singleton singleton02 = Singleton.getInstance();
        singleton02.describe();

        Singleton singleton03 = Singleton.getInstance();
        singleton03.describe();

        System.out.println("Always the same reference");

        System.out.println("######## start Note ########");
        System.out.println("method that update the state of the singleton should be synchronized in order to have consistency in multithreading use cases" );
        System.out.println("######## end Note ########");

        singleton01.increment();
        singleton01.describe();

        singleton02.increment();
        singleton02.describe();







    }
}
