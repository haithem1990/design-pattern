package com.design.pattern.decorator.with;

import com.design.pattern.decorator.decorated.IDrink;

public class WithMilk extends Decorator {

    public WithMilk(IDrink iDrink) {
        super(iDrink);
    }

    public int getPrice() {
        return iDrink.getPrice() + 42;
    }

    public String getDescription() {
        return iDrink.getDescription() + " with milk";
    }
}
