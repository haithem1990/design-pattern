package com.design.pattern.proxy.business;

public class BusinessImpl implements IBusiness {
    public Boolean doBusiness(String reference) {
        System.out.println("BusinessImpl -- > doBusiness");
        if (reference.equals("0")){
                return Boolean.TRUE;
        }
        else
            return Boolean.FALSE;
    }
}
